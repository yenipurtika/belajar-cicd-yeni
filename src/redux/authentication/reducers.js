import { LOG_IN, LOG_OUT } from './actionTypes';

const initialState = {
    // isAuthenticated: false,
    isAuthenticated: localStorage.getItem("keepLogin"), // Activate login after page refresh
};

const authReducer = (state = initialState, action) => {
    if (action.type === LOG_IN) {
        return {
            ...state,
            isAuthenticated: true
        }
    } else if (action.type === LOG_OUT) {
        return {
            ...state,
            isAuthenticated: false,
        }
    }
    return state;
};

export default authReducer;