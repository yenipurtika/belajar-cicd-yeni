import { HANDLE_ADD, HANDLE_MINUS } from './actionTypes';

const initialState = {
    count: 123,
    // hitung: '',
    // data: {},
    // nama: [],
    // isMarried: false,
  };

  const counterReducer = (state = initialState, action) => {
    if(action.type === HANDLE_ADD) {
      return {
        ...state,
        count: state.count + 1,
      } 
    } else if (action.type === HANDLE_MINUS) {
      return {
        ...state,
        count: state.count - 1,
      }
    }
    return state;
  };

export default counterReducer;