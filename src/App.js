import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h4>
          This page was made for TelkomAthon#2 Front-End Project preparation.
        </h4>
        <h4>
          Team Member: <br />
          <a className="App-link" href="https://irfaneffendiahmad.gitlab.io">Ahmad Irfan Effendi</a><br />
          <a className="App-link" href="https://gitlab.com/yenipurtika">Yeni Purtika Simanjuntak</a><br />
          <a className="App-link" href="https://gitlab.com/ariestamiraniaf">Ariesta Mirania Fabiola</a><br />
        </h4>
      </header>
    </div>
  );
}

export default App;
