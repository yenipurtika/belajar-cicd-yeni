import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const ProtectedRoute = ({ isAuth, component: Component, ...rest }) => {
    return (
        <Route 
            {...rest}
            render={routeProps => {
                if(isAuth) {
                    return <Component {...routeProps} />
                } else {
                    return (
                        <Redirect to="/" />
                    )
                }
            }}
        />
    );
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthenticated
});

export default connect (mapStateToProps)(ProtectedRoute);